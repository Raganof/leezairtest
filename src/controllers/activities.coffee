(->
	ActivitiesController = (Activities) ->
		vm = this

		vm.activities = []

		Activities.all 0, 50,
			['productId', 'productName', 'locality', 'advertisedPrice', 'image', 'isActive']
		.then (success) ->
				vm.activities = success.data.data.activities
			, (error) ->
				vm.error = error

		return

	ActivitiesController.$inject = ['Activities']
	angular.module('leezair').controller 'ActivitiesController', ActivitiesController

	return
)()