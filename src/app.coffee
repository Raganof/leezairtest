(->
	angular.module('leezair', [ 'ionic', 'ui.router' ])

		#For the API calls (not used here)
		.value 'leezairConst',
			appId: 'appId'
			appKey: 'appKey'

		.config ($stateProvider, $urlRouterProvider) ->
			$stateProvider
				.state 'activities',
					url: '^/activities'
					templateUrl: 'activities.html'
					controller: 'ActivitiesController'
					controllerAs: 'vm'
				.state 'detail',
					url: '^/activities/{id:int}'
					templateUrl: 'activities.detail.html'
					controller: 'ActivitiesDetailController'
					controllerAs: 'vm'

			# if none of the above states are matched, use this as the fallback
			$urlRouterProvider.otherwise '/activities'
			return

		.config ($ionicConfigProvider) ->
		    $ionicConfigProvider.backButton.text ''
		    return

		.run ($ionicPlatform, $rootScope, $state, $stateParams) ->
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;

			$ionicPlatform.ready ->
				if (window.cordova && window.cordova.plugins.Keyboard)
					cordova.plugins.Keyboard.hideKeyboardAccessoryBar true
				if (window.StatusBar)
					StatusBar.styleDefault()
				return
	return
)()
