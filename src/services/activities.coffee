(->
	ActivitiesFactory = ($http, logger, leezairConst) ->
		APIEndpoint = '/appdata/'

		services =
			all: (page = 0, limit = 50, fields = []) ->
			get: (id, fields = []) ->

		services.all = (page = 0, limit = 50, fields = []) ->
			$http.get APIEndpoint + 'activities.json',
				'params':
					results: (page * limit) + ':' + ((page + 1) * limit)
					appId: leezairConst.appId
					appKey: leezairConst.appKey
					fields: fields
			.then (data) ->
				logger.log 'Successful call to ' + APIEndpoint + 'activities.json'
				return data

		services.get = (id, fields = []) ->
			$http.get APIEndpoint + 'activity-' + id + '.json',
				'params':
					appId: leezairConst.appId
					appKey: leezairConst.appKey
					fields: fields
			.then (data) ->
				logger.log 'Successful call to ' + APIEndpoint + 'activity-' + id +  '.json'
				return data

		return services

	#Manual injection to safeguard against minification.
	ActivitiesFactory.$inject = ['$http', 'logger', 'leezairConst']
	angular.module('leezair').factory 'Activities', ActivitiesFactory

	return
)()