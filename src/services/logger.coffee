(->
	LoggerService = () ->
		services =
			error: (msg) ->
			log: (msg) ->

		services.error = (msg) ->
			console.error msg
		services.log = (msg) ->
			console.log msg
		return services

	angular.module('leezair').factory 'logger', LoggerService

	return
)()