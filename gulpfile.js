var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var sass = require('gulp-sass');
var coffee = require('gulp-coffee');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var jade = require('gulp-jade');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
var clean = require('gulp-clean');
var sh = require('shelljs');

var in_paths = {
  sass: ['./src/views/scss/**/*.scss'],
  coffee: ['./src/**/*.coffee'],
  jade: ['./src/views/**/*.jade']
};
var out_paths = {
  html: './www/',
  js: './www/js/',
  css: './www/css/'
}

var gulp_src = gulp.src;
gulp.src = function() {
  return gulp_src.apply(gulp, arguments)
    .pipe(plumber(function(error) {
      gutil.beep();
      if (error.plugin == 'gulp-coffee')
        gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.stack + ': ' + error.message));
      else
        gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
      this.emit('end');
    })
  );
};

gulp.task('default', ['coffee', 'jade', 'sass']);

gulp.task('sass', function(done) {
  gulp.src(in_paths.sass)
    .pipe(changed(out_paths.css, {extension: '.css'}))
    .pipe(sass())
    .pipe(gulp.dest(out_paths.css))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(out_paths.css))
    .on('end', done);
});

gulp.task('coffee', function(done) {
  gulp.src(in_paths.coffee)
    .pipe(changed(out_paths.js, {extension: '.js'}))
    .pipe(sourcemaps.init())
    .pipe(coffee({bare: true}))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(out_paths.js))
    .on('end', done);
});

gulp.task('jade', function(done) {
  gulp.src(in_paths.jade)
    .pipe(changed(out_paths.html, {extension: '.html'}))
    .pipe(jade())
    .pipe(gulp.dest(out_paths.html))
    .on('end', done);
});

gulp.task('clean', function(done) {
  gulp.src(['./www/*', '!./www/lib', '!./www/img'])
    .pipe(clean())
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(in_paths.sass, ['sass']);
  gulp.watch(in_paths.coffee, ['coffee']);
  gulp.watch(in_paths.jade, ['jade']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});
