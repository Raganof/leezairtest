(->
	ActivitiesDetailController = ($rootScope, $ionicHistory, Activities) ->
		currentId = $rootScope.$stateParams.id
		vm = this

		vm.activity = null
		vm.sliderOptions =
			loop: true
			pager: false

		Activities.get currentId
			.then (success) ->
					vm.activity = success.data.data
				, (error) ->
					$ionicHistory.currentView $ionicHistory.backView() #location replace for $state go doesn't seem to work
					$rootScope.$state.go 'activities', {},
						location: 'replace'
		return

	ActivitiesDetailController.$inject = ['$rootScope', '$ionicHistory', 'Activities']
	angular.module('leezair').controller 'ActivitiesDetailController', ActivitiesDetailController

	return
)()